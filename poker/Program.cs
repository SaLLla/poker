﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace poker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите карты, через пробел");
            string[] CombinationOfCards = Console.ReadLine().ToUpper().Split(new char[] { ' ', '\t' }, 5, StringSplitOptions.RemoveEmptyEntries);
            Cards cards = new Cards();
            cards.Sorting(CombinationOfCards);
            Console.ReadLine();
        }
    }
    class Cards
    {

        public void Sorting(string[] inCombinationOfCards)
        {
            try
            {
                SortByCombination(SortMaxToMin(StringToInt(inCombinationOfCards)));
            }
            catch(NullReferenceException)
            {
                Console.WriteLine("Была введена неизвестная карта. Повторите попытку.");
            }
        }

        int[] StringToInt(string[] inCards)
        {
            int[] IntCombination = new int[5];
            int x = 0;

            foreach (string i in inCards)
            {
                switch (i)
                {
                    case "J": IntCombination[x] = 11; break;
                    case "Q": IntCombination[x] = 12; break;
                    case "K": IntCombination[x] = 13; break;
                    case "A": IntCombination[x] = 14; break;
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                    case "10": IntCombination[x] = Convert.ToInt32(i); break;
                    default: return null;
                }
                x++;
            }
            return IntCombination;
        }

        int[] SortMaxToMin(int[] Sort)
        {
            for (int i = 0; i < Sort.Length; i++)
            {
                int maxInd = i;
                int maxVal = Sort[maxInd];
                for (int j = i + 1; j < Sort.Length; j++)
                {
                    if (maxVal < Sort[j])
                    {
                        maxInd = j;
                        maxVal = Sort[maxInd];
                        Sort[maxInd] = Sort[i];
                        Sort[i] = maxVal;
                    }
                }
            }
            return Sort;
        }

        void SortByCombination(int[] inCombinationOfCards)
        {
            if (StraightFlush(inCombinationOfCards))
            {
                if (inCombinationOfCards[0] == 14)
                {
                    Console.WriteLine("Стрейт Флеш!");
                    return;
                }
                else
                {
                    Console.WriteLine("Стрэйт");
                    return;
                }
            }
            int[] i = NumderOfCoincidences(inCombinationOfCards);
            if (i[0] == 4)
            {
                Console.WriteLine("Каре");
                return;
            }
            if (i[0] == 3 && i[1] == 2)
            {
                Console.WriteLine("Фулл Хаус");
                return;
            }
            if (i[0] == 3)
            {
                Console.WriteLine("Тройка");
                return;
            }
            if (i[0] == 2 && i[1] == 2)
            {
                Console.WriteLine("Две пары");
                return;
            }
            if (i[0] == 2)
            {
                Console.WriteLine("Пара");
                return;
            }
            else
            {
                Console.WriteLine("Беда-беда - комбинация нет.");
                return;
            }
        }

        bool StraightFlush(int[] Sort)
        {
            int j = 0;
            for (int i = 0; i < Sort.Length - 1; i++)
            {
                if (Sort[i] - 1 == Sort[i + 1])
                    j++;
            }
            if (j == 4)
                return true;
            else return false;
        }

        int[] NumderOfCoincidences(int[] Sort)
        {
            int[] result = {1, 1, 1, 1, 1};
            int x = 0;
            for (int i = 0; i < Sort.Length; i++ )
            {
                for (int j = i + 1; j < Sort.Length; j++)
                {
                    if (Sort[i] == Sort[j])
                    {
                        result[x]++;
                        i = j;
                    }
                    else
                    {
                        x++;
                        break;
                    }
                }
            }
            return SortMaxToMin(result);
        }
    }
}
